# TOP ML Projects

This is a toy project that fetches the top ML projects on Github by number of
stars, using a number of different topics. 

Topics: ai, machine-learning, machinelearning, ml

Note that machine-learning has about 2k+ projects with more than 200 stars,
so we can't fetch them all (it's limited at 1k).

