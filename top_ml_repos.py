import requests
from collections import namedtuple
import csv
import sys

from secrets import TOKEN

topics = ["machinelearning", "ai", "ml", "machine-learning"]

repositories = dict()
headers = {
    "Accept": "application/vnd.github.v3+json",
    'Authorization': 'token ' + TOKEN
}

keys = "id full_name html_url description stargazers_count".split(" ")

for topic in topics:
    
    page = 1
    
    while True:

        url = f"https://api.github.com/search/repositories?q=stars:>200 topic:{topic} sort:stars-desc&per_page=100&page={page}"

        print(f"{url}")
       
        resp = requests.get(url, headers=headers)

        print(resp.text)
        
        if resp.status_code != 200 or not resp.json()["items"]:
            break
        
        resp_items = resp.json()["items"]
        
        page_repos = {repo["full_name"]:{key: repo[key] for key in keys} for repo in resp_items} 

        repositories = dict(repositories, **page_repos)
        
        page = page + 1
        
        


with open("top_ml_repos.csv", "w") as out_file:
    writer = csv.DictWriter(out_file, fieldnames=keys, quoting = csv.QUOTE_NONNUMERIC)

    writer.writeheader()
    writer.writerows(sorted(repositories.values(), key=lambda x: x["stargazers_count"], reverse=True))
    